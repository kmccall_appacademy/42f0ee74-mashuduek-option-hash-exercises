# Options Hashes
#
# Write a method `transmogrify` that takes a `String`. This method should
# take optional parameters `:times`, `:upcase`, and `:reverse`. Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```
require 'byebug'
def transmogrify(word, options = {})
  defaults = {
    times: 1,
    upcase: true,
    reverse: true
  }
  optionss = defaults.merge(options)
  str = ''
  optionss[:times].times do |wor|
    if optionss.has_key?(:upcase) && optionss.has_key?(:reverse)
      str << word.upcase.reverse
    elsif optionss.has_key?(:reverse)
      str << word.reverse
    elsif optionss.hash_key?(:upcase)
      str << word.upcase
    end
  end
  str
end
